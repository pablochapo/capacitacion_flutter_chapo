import 'package:capacitacion_flutter_gran_chapo/src/Widgets/layout-view.dart';
import 'package:flutter/material.dart';

class Test extends StatelessWidget {
  static const String name = "Test";
  const Test({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutView(
        titleAppbar: 'Vista chingona', body: bodyWidget(), appbar: true);
  }
}

Widget bodyWidget() {
  return SingleChildScrollView(
    child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: 100,
              height: 100,
              color: Colors.red,
              margin: const EdgeInsets.all(10),
            ),
            Container(
              width: 100,
              height: 100,
              color: Colors.red,
              margin: const EdgeInsets.all(10),
            ),
            Container(
              width: 100,
              height: 100,
              color: Colors.red,
              margin: const EdgeInsets.all(10),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: 100,
              height: 100,
              color: Colors.red,
              margin: const EdgeInsets.all(10),
            ),
            Container(
              width: 100,
              height: 100,
              color: Colors.red,
              margin: const EdgeInsets.all(10),
            ),
            Container(
              width: 100,
              height: 100,
              color: Colors.red,
              margin: const EdgeInsets.all(10),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: 100,
              height: 100,
              color: Colors.red,
              margin: const EdgeInsets.all(10),
            ),
            Container(
              width: 100,
              height: 100,
              color: Colors.red,
              margin: const EdgeInsets.all(10),
            ),
            Container(
              width: 100,
              height: 100,
              color: Colors.red,
              margin: const EdgeInsets.all(10),
            ),
          ],
        ),
      ],
    )),
  );
}
