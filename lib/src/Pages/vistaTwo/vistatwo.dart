// ignore: prefer_const_literals_to_create_immutables
// ignore_for_file: prefer_const_constructors

import 'package:capacitacion_flutter_gran_chapo/src/Pages/NewHome/newhome.dart';
import 'package:capacitacion_flutter_gran_chapo/src/Pages/Perfil/Widgets/Header.dart';
import 'package:capacitacion_flutter_gran_chapo/src/Pages/Perfil/Widgets/List-Details.dart';
import 'package:capacitacion_flutter_gran_chapo/src/Widgets/layout-view.dart';
import 'package:flutter/material.dart';

class VistaTwo extends StatefulWidget {
  static const String name = "VistaTwo";
  const VistaTwo({Key? key}) : super(key: key);

  @override
  State<VistaTwo> createState() => _VistaTwoState();
}

class _VistaTwoState extends State<VistaTwo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("ItemOne"),
      ),
      body: Column(
        children: [
          ListTile(
            leading: Icon(Icons.ac_unit_outlined),
            trailing: Icon(Icons.access_alarm),
            onTap: () {
              print("elemento select");
            },
            subtitle: Text("subtitulo"),
            title: Text("Elemento 1"),
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.ac_unit_outlined,size: 20,color: Colors.amberAccent),
            trailing: CircleAvatar(radius: 10,backgroundColor: Colors.green,),
            onTap: () {
              print("elemento select");
            },
            subtitle: Text("subtitulo"),
            title: Text("Elemento 2"),
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.ac_unit_outlined),
            trailing: Icon(Icons.access_alarm),
            onTap: () {
              Navigator.pushNamed(context, NewHomeView.name );
              print("elemento select");
            },
            subtitle: Text("subtitulo"),
            title: Text("Elemento 3"),
          ),
          Divider(),
        ],
      ),
    );
  }
}


