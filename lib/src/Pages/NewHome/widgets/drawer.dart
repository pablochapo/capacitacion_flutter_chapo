import 'package:capacitacion_flutter_gran_chapo/src/Pages/Login/Screens/Login-View.dart';
import 'package:capacitacion_flutter_gran_chapo/src/Pages/VistaUno/itemone.dart';
<<<<<<< HEAD
import 'package:capacitacion_flutter_gran_chapo/src/Pages/vistaTwo/vistatwo.dart';
=======
import 'package:capacitacion_flutter_gran_chapo/src/Pages/vistaTest/test.dart';
>>>>>>> 70d96be5a8d8ee8c6d139ca01ba855be17a0aa13
import 'package:flutter/material.dart';



class DrawerView extends StatefulWidget {
  const DrawerView({Key? key}) : super(key: key);

  @override
  _DrawerViewState createState() => _DrawerViewState();
}

class _DrawerViewState extends State<DrawerView> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: ListView(children: [
      const DrawerHeader(
        decoration: BoxDecoration(
          color: Colors.blue,
        ),
        child: Text('Drawer Header'),
      ),
      ListTile(
        title: const Text('Item 1'),
        onTap: () {
          Navigator.pushNamed(context, VistaOne.name);
        },
      ),
      ListTile(
        onTap: (){
           Navigator.pushNamed(context, VistaTwo.name);
        },
        title: Text('Item 2'),
      ),
      ListTile(
        onTap: (){
           Navigator.pushNamed(context, Test.name);
        },
        title: Text('Item 3'),
      ),
    ]));
  }
}
