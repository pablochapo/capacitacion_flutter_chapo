import 'package:capacitacion_flutter_gran_chapo/src/Pages/Perfil/Widgets/Header.dart';
import 'package:capacitacion_flutter_gran_chapo/src/Pages/Perfil/Widgets/List-Details.dart';
import 'package:capacitacion_flutter_gran_chapo/src/Widgets/layout-view.dart';
import 'package:flutter/material.dart';

class VistaOne extends StatelessWidget {
  static const String name = "VistaOne";
  const VistaOne({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
      return Scaffold(
        appBar: AppBar(
          title: Text("Tilin"),
        ),
        body: Container(
    width: double.infinity,
    height: double.infinity,
    color: Colors.red,
    child: Center(
        child: Container(
          color: Colors.black45,
          width: double.infinity,
          height: double.infinity,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Row(
                 crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
      children: [
              Column(
                children: [
                  Container(
                    margin: EdgeInsets.all(10),
                    padding: EdgeInsets.only(left: 50),
                    color: Colors.black,
                    width: 80,
                    height: 80,
                    child: Container(
                      color: Colors.blue,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(10),
                    color: Colors.black,
                    width: 80,
                    height: 80,
                  ),
                ],
              ),
              Column(
                children: [
                  Container(
                    margin: EdgeInsets.all(10),
                    color: Colors.black,
                    width: 80,
                    height: 80,
                  ),
                  Container(
                    margin: EdgeInsets.all(10),
                    color: Colors.black,
                    width: 80,
                    height: 80,
                  ),
                ],
              ),
      ],
    ),
            ],
          ),
        )),
  ),
      );
}

Widget bodyWidget() {
  return Container(
    width: double.infinity,
    height: double.infinity,
    color: Colors.red,
    child: Center(
        child: Row(
      children: [
        Column(
          children: [
            Container(
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.only(left: 50),
              color: Colors.black,
              width: 80,
              height: 80,
              child: Container(
                color: Colors.blue,
              ),
            ),
            Container(
              margin: EdgeInsets.all(10),
              color: Colors.black,
              width: 80,
              height: 80,
            ),
          ],
        ),
        Column(
          children: [
            Container(
              margin: EdgeInsets.all(10),
              color: Colors.black,
              width: 80,
              height: 80,
            ),
            Container(
              margin: EdgeInsets.all(10),
              color: Colors.black,
              width: 80,
              height: 80,
            ),
          ],
        ),
      ],
    )),
  );
}
}